﻿using UnityEngine;

[CreateAssetMenu(fileName = "Dialogue", menuName = "Zelda/Dialogue", order = 1)]
public class Dialogue : ScriptableObject
{
    [SerializeField]
    private string m_Text = string.Empty;

    [SerializeField]
    private string m_SpeakerAvatar = null;

    public string GetText() {
        return m_Text;
    }

    public string GetSpeaker()
    {
        return m_SpeakerAvatar;
    }


    private void OnMouseDown() 
    {
        Debug.Log(GetText()); 
    }
}
