﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    private Animator m_Animator = null;
    private bool m_isWalking = false;

    [SerializeField]
    private float m_sightRange = 10f;

    [SerializeField]
    private float m_Speed = 6.24f;


    private void Awake() {
        m_Animator = GetComponent<Animator>(); 
    }

    ///call each frames
    private void Update() {
        Vector3 movement = Vector3.zero;
        movement.x = Input.GetAxisRaw("Horizontal");
        movement.z = Input.GetAxisRaw("Vertical");
        movement.Normalize();


        if (Input.GetKeyDown(KeyCode.DownArrow)) {
            m_Animator.SetTrigger("Down");
        }

         if(Input.GetKeyDown(KeyCode.LeftArrow)) {
            m_Animator.SetTrigger("Left");
        }

         if(Input.GetKeyDown(KeyCode.UpArrow)) {
            m_Animator.SetTrigger("Back");
        }

         if(Input.GetKeyDown(KeyCode.RightArrow)) {
            m_Animator.SetTrigger("Right");
        }

        m_isWalking = true;

        if(Input.GetKey(KeyCode.DownArrow)) { }
        else if(Input.GetKey(KeyCode.LeftArrow)) {}
        else if(Input.GetKey(KeyCode.UpArrow)) {}
        else if(Input.GetKey(KeyCode.RightArrow)) {} 
        else {
            m_isWalking = false;
        }

        transform.position += movement * m_Speed * Time.deltaTime;
        m_Animator.SetBool("isWalking", m_isWalking);

    }

}
